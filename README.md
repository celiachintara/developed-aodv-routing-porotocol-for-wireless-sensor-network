# Developed AODV Routing Porotocol for Wireless Sensor Network

Kelompok 1 : <br>
Celia Chintara Yuwine (05111740000058) <br>
Salsha Armenia Amosea (05111740000142) <br>
Ayu Mutiara Sari (05111740000149) <br>
<br> 

## 1. Konsep <br>
### 1.1 Deskripsi Paper <br>
Implementasi Developed AODV didasarkan pada paper berikut : <br>
*  Judul : Developed Ad Hoc On-demand Distance Vector Routing Protocol for Wireless Sensor Networks
*  Penulis : Muayad Al-Janabi , Radhwan Kadhim
*  Tahun Terbit : 2018
*  Sumber : [Developed Ad Hoc On-demand Distance Vector Routing Protocol for Wireless Sensor Networks](ttps://www.researchgate.net/publication/324214374_Developed_Ad_Hoc_On-demand_Distance_Vector_Routing_Protocol_for_Wireless_Sensor_Networks) <br>
  

### 1.2 Latar Belakang <br>
Wireless Sensor Network terdiri dari node-node sensor dalam jumlah besar yang terhubung dalam suatu mode ad hoc dan dapat didefinisikan sebagai self-configured, 
decentralized, dan tanpa infrastructure. Wireless Sensor Network digunakan untuk memonitor material atau kejadian pada lingkungan, seperti kelembapan, getaran, 
suhu, tekanan, gerakan atau polutan. Selain itu, juga digunakan untuk mengirimkan data melalui jaringan menuju stasiun pangkal atau menghilangkan beberapa node 
melalui multi-hop environment. <br>
Dikarenakan keterbatasan fitur dari Wireless Sensor Network Ad-Hoc, seperti kerusakan hardware, faktor lingkungan, dan kerusakan path, maka dibutuhkan suatu 
routing protocol yang lebih memumpuni dan juga dapat memenuhi kebutuhan Quality of Service (QoS).<br>
Untuk memenuhi hal tersebut, paper ini mengusulkan sebuah routing protocol baru yaitu **Developed AODV for Wireless Sensor Network**, dimana routing protocol ini 
mengimplementasikan metode *Multi-Objective Optimization* a.k.a *Pareto Optimal Solution*. Routing protocol ini dapat 
meningkatkan bit rate, meminimalisir delay dan packet loss, dan juga mampu menghemat daya baterai karena dapat memilih beberapa path sebagai alternatif jika 
terjadi kegagalan pada saat proses routing, sehingga tidak perlu untuk melakukan reroute discovery dari awal.<br>


### 1.3 Dasar Teori <br>
#### 1.3.1 Ad Hoc On-demand Distance Vector Routing Protocol (AODV Routing Protocol) <br>
**AODV** adalah routing protocol reaktif di mana rute dibuat hanya jika diperlukan. <br>
Dalam AODV, dikenal 3 jenis paket untuk route discovery dan route maintenance nya, yaitu : <br>
*  Route Request (RREQ) <br>
Formatnya adalah seperti gambar di bawah ini : <br>
![rreq](/uploads/5444131f4e9e1daa5695a3d0d0811d07/rreq.jpg) <br>
Keterangan : <br>
    - Type : Tipe paket (RREQ = 1)
    - |J|R|G|D|U| : Flag yang menunjukkan keadaan paket
    - Reserved : Default 0, diabaikan
    - Hop Count : Jumlah hops dari node asal ke node yang sedang meng-handle RREQ saat ini
    - RREQ ID : Nomor unik untuk mengidentifikasi RREQ tertentu
    - Destination IP Address : Alamat IP tujuan
    - Destination Sequence Number : Urutan node tujuan
    - Originator IP Address : Alamat IP asal yang menginisialisasi RREQ
    - Originator Sequence Number : Urutan node asal <br><br>

* Route Reply (RREP) <br>
Formatnya adalah seperti gambar di bawah ini : <br>
![rrep](/uploads/7e04ba2cdd9d061c3bf8cc1a8fd5be84/rrep.jpg)<br>

* Route Error (RERR) <br>
Formatnya adalah seperti gambar di bawah ini : <br>
![rerr](/uploads/a8e37ae4071f0014a1aa4b756df4f14c/rerr.jpg) <br>

**Cara Kerja AODV :** <br>
* Ketika node sumber ingin mengirimkan data ke node tujuan, pertama-tama node tersebut akan 
memeriksa routing table nya, jika ada path-nya, maka node akan menggunakan path itu, jika tidak, 
ia memulai proses routing dengan membroadcast paket RREQ ke node tetangganya. Ketika sebuah node
menerima paket RREQ, node tersebut akan cek apakah RREQ yang diterima sudah ada atau belum, untuk 
mencegah perulangan/duplikat.
* Paket RREQ berisikan source IP address, a source sequence number, a broadcast ID, a destination 
IP address, a destination sequence number dan hop count.
* Saat menerima paket RREQ, setiap node akan menyertakan baris baru yang berisi node asal dalam routing table. 
* Broadcast paket RREQ akan terus berlanjut sampai tiba ke node tujuan. Saat sudah sampai di node tujuan, 
ia akan mengirimkan Route Reply Packet (RREP) ke node sumber, dan setiap node sebelumnya akan mengatur routing 
table dengan menambahkan entri baru yang berisikan IP address, a hop count and a lifetime. Sehingga, setelah 
node sumber menerima paket RREP, pesan yang ingin disampaikan dari node asal ke tujuan dapat dikirim dengan menggunakan data routing table.
* Jika terdeteksi ada kegagalan dalam proses routing seperti connection failed, maka node akan mengirimkan pesan 
* Rute Kesalahan (RERR) ke node sumber dan proses routing akan diulang kembali dari awal.<br><br>
Ilustrasi proses pembentukan rute pada AODV dengan menggunakan pesan RREQ dan RREP dapat dilihat pada gambar di bawah ini <br>
![boardcast_RREQ](/uploads/4821a721de67b0a7fdf08733e8d1eb68/boardcast_RREQ.jpg)<br>
Pengiriman Route Request (RREQ)<br><br>
![unicast_RREP](/uploads/9767ed0f276e058b41e37ffd990230a2/unicast_RREP.jpg)<br>
Pengiriman Route Reply (RREP)<br><br>

#### 1.3.2 Multi-Objective Optimization<br>
Masalah dalam Multi-objective Optimization mengoptimalkan (meminimalkan semua objective function / memaksimalkan semua objective
function / meminimalkan sebagian objective function dan memaksimalkan lainnya) multi-objective function sesuai dengan rangkaian kendala
yang diberikan oleh masalah yang akan dioptimalkan. Oleh karena itu, akan ditemukan serangkaian solusi untuk objective function yang bertentangan.<br>
Masalah dalam Multi-objective optimization dapat dijabarkan dalam bentuk matematika sebagai berikut : <br>
![masalah_multiobjective_opt](/uploads/8bbeeb2af5cc91d4b51dd0a4185b3a5a/masalah_multiobjective_opt.jpg)<br>
Dimana : <br>
![definisi_x](/uploads/2dc0afec7344bf3fd4044fe680e60f67/definisi_x.jpg) adalah vektor dari variable yang harus dioptimalkan , <br>
![definisi_fj](/uploads/fa7f03d450f068d8ad8b923cd0d7661e/definisi_fj.jpg) adalah objective function, <br>
![definisi_gi](/uploads/453ba60ecd8c91293fa740ee87ae05e0/definisi_gi.jpg) adalah constraint pada function.<br><br>

Masalah yang dapat terjadi dengan Multi-objective function yaitu dapat terjadinya konflik antara function-function tersebut. Misalnya,
ketika kita meningkatkan satu objective function, maka objective function lainnya dapat memburuk. Demikian, tidak ada satu pun solusi
yang dapat mengoptimalkan seluruh objective function secara bersamaan. Maka dari itu, solusi terbaik untuk saat ini yaitu *Pareto Optimal Solutions*,
yang diusulkan oleh Edgeworth dan Pareto, dan secara resmi didefinisikan dalam kasus minimization sebagai berikut : <br>
![minimization_case](/uploads/87777f67cdadab86754283203058ac78/minimization_case.jpg)<br>

Target dari metode Multi-objective Optimization adalah untuk menghasilkan beberapa solusi optimal Pareto untuk memudahkan pengguna mengevaluasi trade-offs 
antara beberapa objective yang mengalami konflik.<br><br>

### 1.4 Routing Protocol yang Diusulkan<br>
Untuk memperbaiki routing protocol AODV dan memenuhi QoS yang dibutuhkan pada saat ini, maka paper ini mengusulkan sebuah routing protocol
baru yaitu **Developed AODV for Wireless Sensor Network**.<br>
* Tujuan dari Developed AODV for Wireless Sensor Network : <br>
    * Memilih jalur terpendek dengan memperhatikan 3 metrices : memaksimalkan bit rate, meminimalkan packet loss dan delay time. <br>
    * Memilih jalur lainnya sebagai alternatif path apabila terjadi kegagalan pada saat proses routing discovery.<br><br>

* Untuk memenuhi tujuan tersebut, terdapat beberapa formula untuk perhitungan dalam proses routing discovery, antara lain :<br>
    *  **A. Modeling of the Network**<br>
    Ad Hoc untuk Wireless Sensor Network (WSN) dapat dimodelkan sebagai *undirectred graph*.<br>
    **G = (V, E, Q)** dengan node sebanyak **N** <br>
    Dimana :<br>
    V menotasikan kumpulan *vertex* yang merepresentasikan node-node,<br>
    E menotasikan kumpulan *edge* yang merepresentasikan *links* diantara node-node,<br>
    Q adalah vektor Quality of Service untuk setiap *link*.<br><br>

    * **B. Packet Loss Rate Metric**<br>
    Packet loss rate digunakan untuk mengukur probabilitas kegagalan transmisi dan dapat dinyatakan dalam bentuk rasio pengiriman data (delivery ratio).<br>
    rasio pengiriman data (*Packet Loss Rate*) dapat digambarkan sebagai berikut : <br>
    ![packet_loss_rate_metric](/uploads/e0e3d5bddc303f35f2a6ded48c25d772/packet_loss_rate_metric.PNG) <br>
    ![PLR_link](/uploads/c80decf7be5eb61656a6cc8cdd577049/PLR_link.PNG) adalah notasi dari rasio pengiriman data (*packet loss rate*) pada setiap *link*. <br>
    ![pkti](/uploads/fb02e8cf6cd0e37fd2b1cf9daa53f199/pkti.PNG) adalah notasi dari jumlah total paket yang diterima pada node *j* <br>
    ![Pktj](/uploads/063cb9d8075bebafdc4c949e1a37e8f6/Pktj.PNG) adalah notasi dari jumlah paket yang dihasilkan oleh node *i* <br>
    lalu, kita asumsikan bahwa  ![plrlinki](/uploads/27bf55df6773880bdbd8554e15358f9e/plrlinki.PNG) adalah nilai dari *packet loss rate* di setiap *link*, jadi *end-to-end packet loss rate* untuk jalur p, dapat dihitung dengan: <br>
    ![e2e_packet_loss](/uploads/7895e93ed515d22e0104ec09b2af3f2b/e2e_packet_loss.PNG) <br><br>
    
    * **C. Bit Rate Metric**<br>
    Bit rate adalah jumlah bits yang dihasilkan atau diproses pada suatu waktu. *end-to-end bit rate* untuk jalur p, dapat dihitung dengan: <br>
    ![bitrate](/uploads/48d40d5b552e90ad49991e1fbf3c6d9d/bitrate.PNG) <br><br>
    
    * **D. Delay Metric**<br>
    Delay adalah waktu yang dihabiskan dari perjalanan paket data dari sumber ke node tujuan. Banyak aplikasi WSN yang sensitif terhadap *delay time* pada real-time event. maka perlu dilakukan teknik routing yang efisien dalam mengurangi keterlambatan dalam pengiriman paket data <br>
    *end-to-end delay time* pada jalur p, adalah penjumlahan dari *delay* antara semua *intermediate* node sepanjang jalur p, dinotasikan sebagai berikut: <br>
    ![delay](/uploads/625030bc5e3a608197a6aaa8886b2e26/delay.PNG) <br><br>
    
   ### 1.5 Cara Kerja <br>
   ![edited_flowchart](/uploads/e398473b4610c6ff766968fb3a6f2fd9/edited_flowchart.png)<br>
    ketika node asal ingin mengirim data ke node tujuan, Pertama, node asal akan mengecek pada routing table apakah jalur ke destination node tersedia.
    jika jalur ada, maka bisa langsung dipakai, sebaliknya jika tidak, maka dilakukan *route discovery*. *Route Discovery* dilakukan dengan mem-*broadcast* RREQ packet ke node tetangga.
    setelah node asal menemukan jalur pertama ke node tujuan, lalu node asal menggunakan Yen's algorithm untuk mencari kemungkinan jalur lain pada node yang sama. Untuk semua path diantara node asal dan node tujuan,
    kita menghitung parameter QoS (*packet loss rate metric, bit rate metric,* dan *delay metric*). Setelah itu, node tujuan berisi semua informasi RREQ (dan informasi parameter QoS) membuat table yang disebut *source decision table*<br>
    ![source_decision_table](/uploads/47fd3083616ae026d1db1a176ed0de96/source_decision_table.PNG)<br>
    setelah itu, node tujuan memilih jalur yang kuat pada table yang memenuhi fungsi: <br>
    ![fungsi_cara_kerja](/uploads/deadf4e71e873f9708d25e8c078af7c1/fungsi_cara_kerja.PNG) <br>
    dimana p adalah indeks dari nomer jalur.<br>
    Terakhir, kita menggunakan metode *weighted sum multi-objective optimization* untuk mencari solusi optimal menggunakan Pareto. <br><br>
    
## 2. Implementasi <br>
### 2.1 Deskripsi Sistem <br>
Implementasi Developed AODV Routing Protocol for Wireless Sensor Network dilakukan pada: <br>
    
*   Sistem Operasi: Ubuntu 18.04 <br>
*  Aplikasi yang digunakan: NS-2 [Instalasi](intip.in/WORKSHOPNS2) <br>
*  Bahasa C++ dengan memodifikasi algoritma AODV yang sudah ada <br>
*  Simulation Environment : <br>
*  Testing: <br><br>
    
## 3. Referensi <br>
 
*  [Developed Ad Hoc On-demand Distance Vector Routing Protocol for Wireless Sensor Networks](ttps://www.researchgate.net/publication/324214374_Developed_Ad_Hoc_On-demand_Distance_Vector_Routing_Protocol_for_Wireless_Sensor_Networks)


